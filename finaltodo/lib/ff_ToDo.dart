import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

late Database database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = await openDatabase(
    join(await getDatabasesPath(), "ToDoDB.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''
        CREATE TABLE ToDoData(
          title TEXT PRIMARY KEY,
          description TEXT,
          date INTEGER
        )
      ''');
    },
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDoData(),
    );
  }
}

class ToDoData extends StatefulWidget {
  const ToDoData({Key? key}) : super(key: key);

  @override
  State<ToDoData> createState() => _ToDoDataState();
}

class _ToDoDataState extends State<ToDoData> {
  final TextEditingController dateController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  late List<ToDoModel> tasks = [];

  bool _isdbInitialized = false;

  @override
  void initState() {
    super.initState();
    initializeDatabase();
  }

  Future<void> initializeDatabase() async {
    if (!_isdbInitialized) {
      final localDB = database;
      final List<Map<String, dynamic>> data = await localDB.query('ToDoData');

      setState(() {
        _isdbInitialized = true;
        tasks = data.map((taskMap) => ToDoModel.fromMap(taskMap)).toList();
      });
    }
  }

  Future<void> insertTaskData(ToDoModel task) async {
    final localDB = database;
    await localDB.insert(
      'ToDoData',
      task.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    setState(() {
      tasks.add(task);
    });
  }

  Future<void> deleteTaskData(String title) async {
    final localDB = database;
    await localDB.delete(
      'ToDoData',
      where: 'title = ?',
      whereArgs: [title],
    );

    setState(() {
      tasks.removeWhere((task) => task.title == title);
    });
  }

  Future<void> showBottomSheet(BuildContext context, bool doEdit, [ToDoModel? todoModelObj]) async {
    await showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(30),
        ),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
            left: 15,
            right: 15,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  doEdit ? "Edit Task" : "Create Task",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Form Fields
                      TextFormField(
                        controller: titleController,
                        decoration: const InputDecoration(
                          hintText: "Title",
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: descriptionController,
                        decoration: const InputDecoration(
                          hintText: "Description",
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a description';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: dateController,
                        readOnly: true,
                        decoration: const InputDecoration(
                          hintText: "Date",
                          suffixIcon: Icon(Icons.date_range_rounded),
                        ),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2023),
                            lastDate: DateTime(2025),
                          );
                          if (pickedDate != null) {
                            String formattedDate = DateFormat.yMMMd().format(pickedDate);
                            dateController.text = formattedDate;
                          }
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please select a date';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      if (doEdit) {
                        // Edit Task Logic
                      } else {
                        final newTask = ToDoModel(
                          title: titleController.text,
                          description: descriptionController.text,
                          date: DateFormat.yMMMd().parse(dateController.text),
                        );
                        insertTaskData(newTask);
                      }
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text(doEdit ? "Update" : "Create"),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

 @override
  Widget build(BuildContext context) {

    return Scaffold(

      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),

      body: Padding(

        padding: const EdgeInsets.only(top: 100),

        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,

          children: [

            Container(

              padding: const EdgeInsets.only(left: 35),

              child: Text(
                "Good Morning",
                style: GoogleFonts.quicksand(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  fontSize: 22,
                ),
              ),
            ),

            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.only(left: 35),
              child: Text(
                "Tejaswini",
                style: GoogleFonts.quicksand(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 30,
                ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                width: double.infinity,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      "CREATE TASKS",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),

                    Expanded(

                      child: Container(

                        padding: const EdgeInsets.only(top: 20),
                        decoration: const BoxDecoration(

                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),

                        child: tasks.isEmpty
                            ? const Center(child: Text('No tasks available'))
                            : ListView.builder(

                                scrollDirection: Axis.vertical,
                                itemCount: tasks.length,
                                itemBuilder: (context, index) {

                                  final task = tasks[index];
                                  final colors = [
                                    const Color.fromRGBO(255, 255, 255, 1),
                                    const Color.fromRGBO(232, 237, 250, 1),
                                    const Color.fromRGBO(255, 255, 255, 1),

                                    const Color.fromRGBO(250, 232, 250, 1),
                                    const Color.fromRGBO(255, 255, 255, 1),

                                    const Color.fromRGBO(250, 232, 232, 1),

                                  ];
                                  final color = colors[index % colors.length];

                                  //Slidable
                                  return Slidable(

                                    closeOnScroll: true,
                                    endActionPane: ActionPane(

                                      extentRatio: 0.2,
                                      motion: const DrawerMotion(),

                                      children: [

                                        Expanded(

                                          child: Column(

                                            mainAxisAlignment: MainAxisAlignment.spaceAround,

                                            children: [
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              GestureDetector(

                                                onTap: () {
                                                  titleController.text = task.title;
                                                  descriptionController.text = task.description;
                                                  dateController.text = DateFormat.yMMMd().format(task.date);
                                                  showBottomSheet(context, true, task);
                                                },

                                                child: Container(
                                                  padding: const EdgeInsets.all(10),
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                    color: const Color.fromRGBO(89, 57, 241, 1),
                                                    borderRadius: BorderRadius.circular(20),
                                                  ),

                                                  child: const Icon(

                                                    Icons.edit,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),

                                              const SizedBox(
                                                height: 20,
                                              ),

                                              GestureDetector(

                                                onTap: () {
                                                  deleteTaskData(task.title);
                                                },

                                                child: Container(

                                                  padding: const EdgeInsets.all(5),
                                                  height: 40,
                                                  width: 40,

                                                  decoration: BoxDecoration(

                                                    color: const Color.fromRGBO(89, 57, 241, 1),
                                                    borderRadius: BorderRadius.circular(20),
                                                  ),

                                                  child: const Icon(

                                                    Icons.delete,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),

                                              const SizedBox(
                                                height: 5,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),

                                    key: ValueKey(index),

                                    child: Container(

                                      margin: const EdgeInsets.only(top: 10),
                                      padding: const EdgeInsets.only(left: 20, bottom: 20, top: 20),

                                      decoration: BoxDecoration(

                                        color: color,
                                        border: Border.all(color: const Color.fromRGBO(0, 0, 0, 0.05), width: 0.5),

                                        boxShadow: const [

                                          BoxShadow(
                                            offset: Offset(0, 4),
                                            blurRadius: 20,
                                            color: Color.fromRGBO(0, 0, 0, 0.13),
                                          ),
                                        ],

                                        borderRadius: const BorderRadius.all(Radius.zero),
                                      ),

                                      child: Row(

                                        crossAxisAlignment: CrossAxisAlignment.start,

                                        children: [

                                          Container(

                                            height: 60,
                                            width: 60,

                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Color.fromRGBO(217, 217, 217, 1),
                                            ),

                                            child: Image.asset(
                                              "assets/images/img.png",
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 20,
                                          ),

                                          SizedBox(
                                            width: 220,

                                            child: Column(

                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  task.title,
                                                  style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 15,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  task.description,
                                                  style: GoogleFonts.inter(
                                                    color: const Color.fromRGBO(0, 0, 0, 0.7),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 5,
                                                ),
                                                Text(
                                                  DateFormat.yMMMd().format(task.date),
                                                  style: GoogleFonts.inter(
                                                    color: const Color.fromRGBO(0, 0, 0, 0.7),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          Checkbox(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            activeColor: Colors.green,
                                            value: true,
                                            onChanged: (val) {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(

        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        onPressed: () async {

          await showBottomSheet(context, false);
        },

        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}

class ToDoModel {

  final String title;
  final String description;
  final DateTime date;

  ToDoModel({
    required this.title,
    required this.description,
    required this.date,
  });

  Map<String, dynamic> toMap() {

    return {
      'title': title,
      'description': description,
      'date': date.millisecondsSinceEpoch,
    };
  }

  factory ToDoModel.fromMap(Map<String, dynamic> map) {

    return ToDoModel(
      title: map['title'],
      description: map['description'],
      date: DateTime.fromMillisecondsSinceEpoch(map['date']),
    );
  }
}
