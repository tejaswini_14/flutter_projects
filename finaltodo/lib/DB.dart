import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

dynamic database;

class ToDoData {
  final String title;
  final String description;
  final int date;

  ToDoData({
    required this.title,
    required this.description,
    required this.date,
  });

  Map<String, dynamic> employeeMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
    };
  }
}

Future<void> insertEmployeeData(ToDoData data) async {
  final localDB = await database;
  localDB.insert(
    'ToDoData',
    data.employeeMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<List<Map<String, dynamic>>> getEmployeeData() async {
  final localDB = await database;
  List<Map<String, dynamic>> mapEntry = await localDB.query("ToDoData");
  return mapEntry;
}

Future<void> deleteEmpData(ToDoData data) async {
  final localDB = await database;
  await localDB.delete(
    'Employee',
    where: "empId = ?",
    whereArgs: [data.title],
  );
}

void main() async {
  runApp(const MainApp());
  database = openDatabase(
    join(await getDatabasesPath(), "ToDoDB.db"),
    version: 1,
    onCreate: (db, version) {
      db.execute('''
        CREATE TABLE ToDoData(
          title TEXT PRIMARY KEY,
          description TEXT,
          date DATE
        )
      ''');
    },
  );

  ToDoData task1 = ToDoData(
    title: "adasf",
    description: "gvtgfv",
    date: 12,
  );
  ToDoData task2 = ToDoData(
    title: "yguyjft",
    description: "qwertyui",
    date: 10,
  );
  await insertEmployeeData(task1);
  await insertEmployeeData(task2);
  List<Map<String, dynamic>> retVal = await getEmployeeData();
  for (int i = 0; i < retVal.length; i++) {
    print(retVal[i]);
  }
  await deleteEmpData(task1);
  print("After Delete");
  retVal = await getEmployeeData();
  for (int i = 0; i < retVal.length; i++) {
    print(retVal[i]);
  }
  ToDoData task3 = ToDoData(
    title: "xdfdkA",
    description: "HSGABUYDS",
    date: 13,
  );
  await insertEmployeeData(task3);
  print("Inserted New Data");
  retVal = await getEmployeeData();

  for (int i = 0; i < retVal.length; i++) {
    print(retVal[i]);
  }
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
