import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Database? database;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = await openDatabase(
    join(await getDatabasesPath(), "ToDoDB.db"),
    version: 1,
    onCreate: (db, version) {
      return db.execute('''
        CREATE TABLE ToDoData(
          taskNo INTEGER PRIMARY KEY AUTOINCREMENT,
          title TEXT,
          description TEXT,
          date INTEGER,
          imagePath TEXT
        )
      ''');
    },
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ToDoData(),
    );
  }
}

class ToDoData extends StatefulWidget {
  const ToDoData({Key? key}) : super(key: key);

  @override
  State<ToDoData> createState() => _ToDoDataState();
}

class _ToDoDataState extends State<ToDoData> {
  final TextEditingController dateController = TextEditingController();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  late List<ToDoModel> tasks = [];
    final _formKey = GlobalKey<FormState>();


  File? _image;
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    initializeDB();
  }

  Future<void> initializeDB() async {
  if (database == null) {
    print('Database is not initialized');
    return;
  }

  final localDB = database!;
  final List<Map<String, dynamic>> data = await localDB.query('ToDoData');

  setState(() {
    tasks = data.map((taskMap) => ToDoModel.fromMap(taskMap)).toList();
  });
}


  Future<void> insertTaskData(ToDoModel task) async {
    final localDB = database;
    await localDB!.insert(
      'ToDoData',
      task.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    setState(() {
      tasks.add(task);
      titleController.clear();
      descriptionController.clear();
      dateController.clear();
      _image = null;
    });
  }

  Future<void> deleteTaskData(int taskNo) async {
    final localDB = database;
    await localDB!.delete(
      'ToDoData',
      where: 'taskNo = ?',
      whereArgs: [taskNo],
    );

    setState(() {
      tasks.removeWhere((task) => task.taskNo == taskNo);
    });
  }

  Future<void> getImage() async {
  final pickedFile = await picker.pickImage(source: ImageSource.gallery);

  setState(() {
    if (pickedFile != null) {
      _image = File(pickedFile.path);
    } else {
      print('No image selected.');
    }
  });
}


  Future<void> showBottomSheet(BuildContext context, bool doEdit, [ToDoModel? todoModelObj]) async {
    await showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(30),
        ),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
            left: 15,
            right: 15,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 20),
                Text(
                  "Create Tasks",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
                const SizedBox(height: 15),
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Title",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(89, 57, 241, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: titleController,
                        decoration: InputDecoration(
                          hintText: "Enter Title",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.red),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 15),
                      Text(
                        "Description",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(89, 57, 241, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: descriptionController,
                        maxLines: 4,
                        decoration: InputDecoration(
                          hintText: "Enter Description",
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide: const BorderSide(),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.red),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a description';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 15),
                      Text(
                        "Date",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(89, 57, 241, 1),
                          fontWeight: FontWeight.w400,
                          fontSize: 15,
                        ),
                      ),
                      const SizedBox(height: 3),
                      TextFormField(
                        controller: dateController,
                        readOnly: true,
                        decoration: InputDecoration(
                          hintText: "Enter Date",
                          suffixIcon: const Icon(Icons.date_range_rounded),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                            borderSide: const BorderSide(
                              color: Color.fromRGBO(89, 57, 241, 1),
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderSide: const BorderSide(),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.red),
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2023),
                            lastDate: DateTime(2025),
                          );
                          if (pickedDate != null) {
                            String formattedDate = DateFormat.yMMMd().format(pickedDate);
                            dateController.text = formattedDate;
                          }
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please select a date';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 15),
                      GestureDetector(
                        onTap: getImage,
                        child: Container(
                          height: 60,
                          width: 60,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromRGBO(217, 217, 217, 1),
                          ),
                          child: _image == null
                              ? const Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Icon(Icons.add),
                                )
                              : CircleAvatar(
                                  backgroundImage: FileImage(_image!),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.all(10),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13),
                      ),
                      backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        final newTask = ToDoModel(
                          taskNo: 1,
                          title: titleController.text,
                          description: descriptionController.text,
                          date: DateFormat.yMMMd().parse(dateController.text),
                          imagePath: _image?.path ?? "", // Save image path
                        );
                        insertTaskData(newTask);
                        Navigator.of(context).pop();
                      }
                    },
                    child: Text(
                      "Submit",
                      style: GoogleFonts.inter(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(111, 81, 255, 1),
      body: Padding(
        padding: const EdgeInsets.only(top: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 35),
              child: Text(
                "Good Morning",
                style: GoogleFonts.quicksand(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  fontSize: 22,
                ),
              ),
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.only(left: 35),
              child: Text(
                "Tejaswini",
                style: GoogleFonts.quicksand(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 30,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Color.fromRGBO(217, 217, 217, 1),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40),
                  ),
                ),
                width: double.infinity,
                child: Column(
                  children: [
                    const SizedBox(height: 30),
                    Text(
                      "CREATE TO DO LIST",
                      style: GoogleFonts.quicksand(
                        fontWeight: FontWeight.w500,
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Expanded(
                      child: Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40),
                          ),
                        ),
                        child: tasks.isEmpty
                            ? const Center(child: Text('No tasks available'))
                            : ListView.builder(
                                scrollDirection: Axis.vertical,
                                itemCount: tasks.length,
                                itemBuilder: (context, index) {
                                  final task = tasks[index];
                                  final colors = [
                                    const Color.fromRGBO(255, 255, 255, 1),
                                    const Color.fromRGBO(232, 237, 250, 1),
                                    const Color.fromRGBO(255, 255, 255, 1),
                                    const Color.fromRGBO(250, 232, 250, 1),
                                    const Color.fromRGBO(255, 255, 255, 1),
                                    const Color.fromRGBO(250, 232, 232, 1),
                                  ];
                                  final color = colors[index % colors.length];
                                  return Slidable(
                                    closeOnScroll: true,
                                    endActionPane: ActionPane(
                                      extentRatio: 0.2,
                                      motion: const DrawerMotion(),
                                      children: [
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            children: [
                                              const SizedBox(height: 5),
                                              GestureDetector(
                                                onTap: () {
                                                  titleController.text = task.title;
                                                  descriptionController.text = task.description;
                                                  dateController.text = DateFormat.yMMMd().format(task.date);
                                                  showBottomSheet(context, true, task);
                                                },
                                                child: Container(
                                                  padding: const EdgeInsets.all(10),
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                    color: const Color.fromRGBO(89, 57, 241, 1),
                                                    borderRadius: BorderRadius.circular(20),
                                                  ),
                                                  child: const Icon(
                                                    Icons.edit,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(height: 20),
                                              GestureDetector(
                                                onTap: () {
                                                  deleteTaskData(task.taskNo!);
                                                },
                                                child: Container(
                                                  padding: const EdgeInsets.all(5),
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                    color: const Color.fromRGBO(89, 57, 241, 1),
                                                    borderRadius: BorderRadius.circular(20),
                                                  ),
                                                  child: const Icon(
                                                    Icons.delete,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(height: 5),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    key: ValueKey(index),
                                    child: Container(
                                      margin: const EdgeInsets.only(top: 10),
                                      padding: const EdgeInsets.only(left: 20, bottom: 20, top: 20),
                                      decoration: BoxDecoration(
                                        color: color,
                                        border: Border.all(color: const Color.fromRGBO(0, 0, 0, 0.05), width: 0.5),
                                        boxShadow: const [
                                          BoxShadow(
                                            offset: Offset(0, 4),
                                            blurRadius: 20,
                                            color: Color.fromRGBO(0, 0, 0, 0.13),
                                          ),
                                        ],
                                        borderRadius: const BorderRadius.all(Radius.zero),
                                      ),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 60,
                                            width: 60,
                                            decoration: const BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Color.fromRGBO(217, 217, 217, 1),
                                            ),
                                            child: task.imagePath.isNotEmpty
                                                ? CircleAvatar(
                                                    backgroundImage: FileImage(File(task.imagePath)),
                                                  )
                                                : const SizedBox(),
                                          ),
                                          const SizedBox(width: 20),
                                          SizedBox(
                                            width: 220,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  task.title,
                                                  style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 15,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                                const SizedBox(height: 5),
                                                Text(
                                                  task.description,
                                                  style: GoogleFonts.inter(
                                                    color: const Color.fromRGBO(0, 0, 0, 0.7),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                const SizedBox(height: 5),
                                                Text(
                                                  DateFormat.yMMMd().format(task.date),
                                                  style: GoogleFonts.inter(
                                                    color: const Color.fromRGBO(0, 0, 0, 0.7),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Checkbox(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                            activeColor: Colors.green,
                                            value: true,
                                            onChanged: (val) {},
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        onPressed: () async {
          await showBottomSheet(context, false);
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}

class ToDoModel {
  int taskNo;
  final String title;
  final String description;
  final DateTime date;
  final String imagePath;

  ToDoModel({
    required this.taskNo,
    required this.title,
    required this.description,
    required this.date,
    required this.imagePath,
  });

  Map<String, dynamic> toMap() {
    return {
      'taskNo': taskNo,
      'title': title,
      'description': description,
      'date': date.millisecondsSinceEpoch,
      'imagePath': imagePath,
    };
  }

  factory ToDoModel.fromMap(Map<String, dynamic> map) {
    return ToDoModel(
      taskNo: map['taskNo'],
      title: map['title'],
      description: map['description'],
      date: DateTime.fromMillisecondsSinceEpoch(map['date']),
      imagePath: map['imagePath'] ?? '', // Provide a default value if imagePath is null
    );
  }
}